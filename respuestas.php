<?php
$i = 9;
$f = 33.5;
$c = 'x';

echo ($i >= 6) && ($c == ‘X’), "<br>";         //resultado false
echo ($i >= 6) || ($c == 12), "<br>";		   //resultado true
echo ($f < 11) && ($i > 100), "<br>";          //resultado false
echo ($c != ‘P’) || (($i + $f) <= 10), "<br>"; //resultado true
echo $i + $f <= 10, "<br>";					   //resultado false
echo $i >= 6 && $c == ‘X’, "<br>";             //resultado false
echo $c != ‘P’ || $i + $f <= 10, "<br>";       //resultado true
?>